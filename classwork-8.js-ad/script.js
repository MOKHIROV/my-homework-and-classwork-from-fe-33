/* const BASE_URL = 'https://ajax.test-danit.com/api/json'
const findUser = function(id) {
    return fetch(`${BASE_URL}/users/${id}`)
    .then((response) => {
    if (response.ok) {
        return response.json();
    }
    throw new Error(response.status);
})
.catch(err => {throw new Error(err.message)});
}
const renderUser = (container) => user => {
    if (!container instanceof HTMLElement) return
    const {name , mail , website} = user
    container.innerHTML =  `
    <p>Name : ${name} <br>
    Mail: <a href="mailto:${mail}">${mail}<a><br>
    Website : <a href='${website}'>${website}</a>
    </p>
    `
    }
    const containerElement = document.querySelector('user');
    const submitBtn = document.querySelector(".form-inline [type='submit']")
    function findUserHandler(event){
    submitBtn.addEventListener("click", (event)=> {
        event.preventDefault();
        const dataInput = document.querySelector('.form-inline[type="text"]')
        if (!dataInput.value) return
        findUser(dataInput.value)
        .then(renderUser(containerElement))
        .catch(err => alert(err.message))

    
    })
    }
    submitBtn.addEventListener('click' , findUserHandler)
console.log((e => e)(12)); */




const BASE_URL = 'https://ajax.test-danit.com/api/json'
const findUser = function (id) {
    return fetch(`${BASE_URL}/users/${id}`)
        .then(response => {
            if (response.ok) {
                return response.json()
            }
            throw new Error(response.status)
        })
        .catch(err => { throw new Error(err) })
}
const renderUser = function (container) {
    return function (user) {
        if (!(container instanceof HTMLElement)) return
        const { name, mail, website } = user
        container.innerHTML = `
    <p>Name : ${name} <br>
    Mail : <a href='mailto:${mail}'>${mail}</a> <br>
    Website : <a href='${website}'>${website}</a>
    </p>
    `
    }
}
const containerElement = document.querySelector('.user')
const submitBtn = document.querySelector('.form-inline [type="submit"]')

function findUserHandler(event) {

    event.preventDefault()
    const dataInput = document.querySelector('.form-inline [type="text"]')
    if (!dataInput.value) return
    findUser(dataInput.value)
        .then(renderUser(containerElement))
        // .then(userFn(user))
        // .then(user => {
        //     const userFn = renderUser(containerElement)
        //     userFn(user)
        // })
        .catch(err => alert(err.message))
}
submitBtn.addEventListener('click', findUserHandler)

console.log(
    (e => e)(12)
)