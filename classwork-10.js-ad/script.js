const BASE_URL = 'http://ajax.test-danit.com/api/json'

const fetchPosts = async function () {
    try {
        const response = await fetch(`${BASE_URL}/posts`)
        if(response.status >= 200 && response.status < 300){
            const data = await response.json()
            return data
        } 
        throw new Error(response.status)
    } catch (error) {
        throw new Error(error)
    }
}
const fetchCommentsById = async function (id) {
    try {
        const response = await fetch(`${BASE_URL}/comments?postId=${id}`)
        if(response.status >= 200 && response.status < 300){
            const data = await response.json()
            return data
        } 
        throw new Error(response.status)
    } catch (error) {
        throw new Error(error)
    }
}