/* const productsPromise = new Promise((resolve, reject) => {
    setTimeout(() => {
      if (Math.random() < 0.5) {
        resolve(["orange", "apple", "pineapple", "melon"]);
      } else {
        reject(new Error("Server Error"));
      }
    }, 2000);
  });
  productsPromise.then((result) => {
    if (Array.isArray(result)) {
      const listTemplate = result.reduce((acc, el, index) => {
        if (index === 0) acc += '<ul>'
        acc += `<li> ${el}</li>`
        if (index === result.length - 1) acc += '</ul>'
        return acc
      }, '')
      return document.querySelector('.loader').innerHTML = listTemplate
    }
  })
    .catch(err => alert(err.name)) */

/*     const userDb = [{userName: 'Max', password: '123345'}, {userName: 'Admin', password: '123'}];

const authenticate = (username, password) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const findedUser = userDb.find((currentUser)=>{
        return currentUser.userName === username
      
      })
      if(!findedUser) {
        reject(new Error('User not found'))
      }
      if(findedUser.password === password) {
        resolve(findedUser)
      } else {
        reject (new Error('Password incorrect'))
      }

    }, 2000);
  });
};

authenticate('Max', '123345')
.then((data)=> console.log(data))
.catch((error)=> console.log('Invalid data')) */


/* const city = [{cityName:'Kyiv' , cityName : 'Lviv' , cityName :'Kharkiv'}];

const getWeather = (cyties) => {
 return new Promise((resolve, reject)=> {
     const findCity = city.find((currentCity)=>{
        return currentCity.cityName === cyties;
     })
     if(findCity.cityName !== cyties) reject(new Error('Not find'))
 })
}
getWeather('Kyiv' , 'Lviv' , 'Kharkiv'); */
const city = ['Kyiv', 'Lviv', 'Kharkiv'];

const getWeather = (cityName) => {
     return new Promise(function (resolve, reject) {
        const cityFinded = city.includes(cityName);
        if (cityFinded) {
            resolve(cityName)
        } else {
            reject(new Error('error'))
        }
    })
}

getWeather('Kyiv' , 'Lviv' , 'Kharkiv')
    .then((data) => console.log(data))
    .catch((err) => console.error('Невідоме місто'))