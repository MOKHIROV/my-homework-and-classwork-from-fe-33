const gulp = require('gulp'),  // подключаем Gulp
    webserver = require('browser-sync').create(), // сервер для работы и автоматического обновления страниц
    rigger = require('gulp-rigger'), // модуль для импорта содержимого одного файла в другой
    sass = require('gulp-sass'), // модуль для компиляции SASS (SCSS) в CSS
    autoprefixer = require('gulp-autoprefixer'), // модуль для автоматической установки автопрефиксов
    cleanCSS = require('gulp-clean-css'), // плагин для минимизации CSS
    uglify = require('gulp-uglify'), // модуль для минимизации JavaScript
    imagemin = require('gulp-imagemin'), // плагин для сжатия PNG, JPEG, GIF и SVG изображений
    del = require('del'), // плагин для удаления файлов и каталогов
    minifyjs = require('gulp-js-minify'),
    concat = require('gulp-concat');
    
    let dist_file = 'dist';
    let source_file = 'src';
    
    let path = {
    build:{
        html: dist_file + "/",
        css: dist_file + "/css/",
        js: dist_file + "/js/",
        img: dist_file + "/img/",
        fonts: dist_file + "/fonts/",
    },
    src:{
        html: source_file + "/index.html",
        css: source_file + "/style/style.scss",
        js: source_file + "/js/**/*.js",
        img: source_file + "/img/**/*.png",
        fonts: source_file + "/style/utils/fonts/*.ttf",
        scss: source_file + "/style/**/*.scss",
    },
    watch:{
        html: source_file + "/**/*.html",
        css: source_file + "/style/style.scss",
        js: source_file + "/js/**/*.js",
        img: source_file + "/img/**/*.png",
        fonts: source_file + "/style/utils/fonts/*.ttf",
    },
    clean:'./' + dist_file + '/'
}

let buildHtml = () => {
  return gulp.src(path.src.html)
    .pipe(rigger())
    .pipe(gulp.dest(path.build.html))
    .pipe(webserver.stream())
}
let buildCss = () => {
    return gulp.src(path.src.css)
    .pipe(sass())
    .pipe(cleanCSS())
    .pipe(gulp.dest(path.build.css))
    .pipe(webserver.stream())
}
let buildJs = () => {
    return gulp.src(path.src.js)
    .pipe(rigger())
    .pipe(gulp.dest(path.build.js))
    .pipe(webserver.stream())
}
let buildImg = () => {
    return gulp.src(path.src.img)
    .pipe(imagemin())
    .pipe(gulp.dest(path.build.img))
    .pipe(webserver.stream())
}
const watcher = () => {
	webserver.init({
		server: {
			baseDir: './' + dist_file + '/',
		},
        port: 3000,
        notify: false
	})
}
gulp.task('build' , gulp.series(buildHtml ,buildImg, buildCss , buildJs  , watcher))

    gulp.watch(path.src.html, buildHtml).on('change', webserver.reload)       
    gulp.watch(path.src.scss, buildCss).on('change', webserver.reload)
	gulp.watch(path.src.js, buildJs).on('change', webserver.reload)
    gulp.watch(path.src.img, buildImg).on('change' , webserver.reload)









