class  Employee{
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    } 
}
class Programmer extends Employee{
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get salary(){
        return this._salary * 3;
    }
    set salary(value) {
        this._salary = value;
    }
}
const employee = new Employee('Genadiy' , 30 , 2000);
const programmer = new Programmer('Vasya' , 18 , 10000, 'English , Russian , Ukranian , Italian');
console.log(employee);
console.log(programmer);
console.log(programmer.salary);