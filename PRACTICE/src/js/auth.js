const BASE_URL = 'https://ajax.test-danit.com/api/v2'

class Auth {
    async signIn(email, password) {
        const response = await fetch(`${BASE_URL}/cards/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email, password })
        })
        const token = await response.text();
        sessionStorage.setItem('token', token)
    }
    getToken() {
        return sessionStorage.getItem('token');
    }
    getAuthStatus() {
        return !!this.getToken();
    }
}

export default new Auth();
