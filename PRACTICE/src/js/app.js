import auth from './auth.js';
import { init as initHeader, renderButton } from './header.js';

const handleCreateCard = () => console.log('Create Card')
const handleLogIn = () => {
    auth.signIn('valerii.m@gmail.com', 'qwerty123456')
        .then(() => {
            console.log(auth.getToken());
            renderButton('js-action-btn', handleCreateCard, handleLogIn)
        })
}

initHeader('header', 'js-action-btn', handleCreateCard, handleLogIn)
