console.log('Task 3');

const user1 = {
    name: "John",
    years: 30
};

let {name, years: age, isAdmin = false} = user1;

console.log(user1);
console.log('name: ' + name);
console.log('years: ' + age);
console.log('isAdmin: ' + false);

console.log('-------------------------------------');
