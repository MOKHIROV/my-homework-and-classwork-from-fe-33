const student = {
    firstName: "Margie",
    lastName: "Sullivan",
    sex: "female",
    grades: {
      html: 90,
      css: 60,
      js: 50,
      python: 45,
    },
    printGrades() {
      for (let key in this.grades) {
        if (this.sex === "male") {
          console.log(`По ${key} ${this.firstName} отримав ${this.grades[key]}`);
        } else {
          console.log(`По ${key} ${this.firstName} отримала ${this.grades[key]}`);
        }
      }
    },
    average: function () {
      let sum = 0;
      let subjectsCount = 0;
      for (const subject in this.grades) {
        sum += this.grades[subject];
        subjectsCount++;
      }
      return sum / subjectsCount;
    },
  };
  const SUBJECTS = ["html","css","python","js"]
  function Student(firstName,lastName,sex,grades){
  
      this.firstName = firstName;
      this.lastName = lastName;
      this.sex = sex;
      // this.grades = grades
      if (grades !== null && typeof grades === "object"){
        this.grades = Object.keys(grades).reduce((acc,key) =>{
          if (SUBJECTS.includes(key)){
            let value = +grades[key];
            // if (typeof value === "Number" &&)
            
            acc[key] = (isNaN(value) || value < 0) 
              ? 0
              : (value > 100) 
                ? 100 
                : value;
          } return acc
        },{})
      }
      else{
        this.grades = {};
        console.error("Input for grades is incorrect")
      };
      this.printGrades = function(){
        for (let key in this.grades) {
          if (this.sex === "male") {
            console.log(`По ${key} ${this.firstName} отримав ${this.grades[key]}`);
          } else {
            console.log(`По ${key} ${this.firstName} отримала ${this.grades[key]}`);
          }
        }
      }
      this.average = function(){
        let sum = 0;
        let subjectsCount = 0;
        for (const subject in this.grades) {
          sum += this.grades[subject];
          subjectsCount++;
        }
        return sum / subjectsCount;
      }
      
  }
  const testStudent1 = new Student('USER1','Smith','male',{html: 1000,
    js: 85,
    milk:35,
    css:true,
    python: 102,});
  
  const testStudent2 = new Student('USER2','Smith','male',{html: 1000,
    js: 86,
    milk:35,
    css:true,
    python: 102,});
  
  const testStudent3 = new Student('USER3','Smith','male',{html: 1000,
    js: 101,
    milk:35,
    css:true,
    python: 102,});
   
  const allStudents = [
      testStudent1,
      testStudent2,
      testStudent3
  ] ;
  function getStudentWithHighestResults(students){
  
  const bestStudent = students.reduce((best,student)=>{
    if (best.score < student.average()){
      best.score = student.average();
      best.student = student;
    }
    return best;
  },{score:0,student:null})
  return `${bestStudent.student.firstName} ${bestStudent.student.lastName} ${bestStudent.score} `
  }
  console.log(getStudentWithHighestResults(allStudents))