/* class Task {
    constructor( title = '' ) {
     this.title = title;
     console.log('Происходит создание задачи');
    }
   };
   
   let task = new Task("Выучить JavaScript");
   
   console.log( typeof task );
   console.log( task.title ); */



   function Person(firstName, lastName, age){
    // this = {}
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;

    // this.makeCoffee = function(){
    //     console.log('Make coffee')
    // }
    
    // return {
    //     firstName: firstName,
    //     lastName: lastName,
    //     age: age,
    //     makeCoffee: function(){...}
    // }
}

Person.prototype.makeCoffee = function(){
    console.log('Make coffee')
}

const person1 = new Person('Ivan', 'Petrov', 23)

console.log(person1.makeCoffee())
console.log(Person.prototype)